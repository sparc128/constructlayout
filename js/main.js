/*
new Swiper('.swiper-container', {

	loop: true,
	navigation: {
		nextEl: '.arrow',
	},
	breakpoints: {
		320: {
			slidesPerView: 1,
			spaceBetween: 20
		},
		541: {
			slidesPerView: 2,
			spaceBetween: 40
		}
	}
});

const menuButton = document.querySelector('.menu-button');
const menu = document.querySelector('.header');
menuButton.addEventListener('click', function () {
	menuButton.classList.toggle('menu-button-active');
	menu.classList.toggle('header-active');
})

*/


const getElement = (tagName, classNames) => {
	const element = document.createElement(tagName);
	// добавим возможность чтобы создавать элементы с классами

	if (classNames) {
		element.classList.add(...classNames);
		// element.className.add('test','hello','world');
	}

	return element;
};

/*
* Функция createHeader - будет создавать весь Header
* т.е. логотип, меню, выпадающее меню, соц.сети
* */
const createHeader = (param) => {
	// функция которая отдельно создает элементы
	const header = getElement('header');
	const container = getElement('div', ['container']);
	const wrapper = getElement('div', ['header']);

	if (param.header.logo) {
		//const logo = wrapper.appendChild(getElement('img',['logo']));
		const logo = getElement('img', ['logo']);
		logo.src = param.header.logo;
		logo.alt = 'Логотип' + param.title;
		wrapper.append(logo);
	}

	if (param.header.social) {
		const socialWrapper = getElement('div', ['social']);
		const allSocial = param.header.social.map(item => {
			const socialLink = getElement('a', ['social-link']);
			socialLink.append(getElement('img', []));
			socialLink.href = item.link;

			return socialLink;
		});
		console.log(allSocial);
		socialWrapper.append(...allSocial);
		wrapper.append(socialWrapper);
	}

	// вставляем container в header
	header.append(container);
	container.append(wrapper);

	return header;
};

// Первый аргумент - селектор - родитель конструктора где все будет формироваться как реакте
const movieConstructor = (selector, options) => {
	const app = document.querySelector(selector);
	app.classList.add('body-app')

	// app.textContent = 'hello';
	// если пользователь не загрузил картинку то надо себя подстраховать и написать условие
	// а так же мы формируем функцию генерации самого header и всех элементов в нем
	// app.append(createHeader(options)); - в options передаем и title и logo и все аналог теги
	if (options.header) {
		app.append(createHeader(options));
	}

};

// в html у нас будет div с классом app - селектор
// надо проверять какие option существуют и эти option передавать надо через запятую
// option это большой объект в который будем передавать header main и т.д.
movieConstructor('.app', {
	title: 'Ведьмак',
	header: {
		// путь пишем относительно файла html
		logo: 'witcher/logo.png',
		social: [
			{
				title: 'Twitter' ,
			    link: 'https://twitter.com',
				image: 'witcher/social/twitter.svg',
			},
			{
				title: 'Instagram',
			    link: 'https://instagram.com',
				image: 'witcher/social/instagram.svg',
			},
			{
				title: 'Facebook',
			    link: 'https://facebook.com',
				image: 'witcher/social/facebook.svg',
			},
		]
	}
});


















